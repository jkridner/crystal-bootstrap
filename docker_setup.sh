#!/bin/bash
#docker build -t jkridner/crystal .
touch /tmp/.docker.xauth
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f /tmp/.docker.xauth nmerge -
#docker run --rm -it -p 8888:8888 -v /tmp/.docker.xauth:/tmp/.docker.xauth -v /tmp/.X11-unix:/tmp/.X11-unix --env DISPLAY=$DISPLAY --env XAUTHORITY=/tmp/.docker.xauth jkridner/crystal /usr/bin/crystal play -p 8888 --binding 0.0.0.0
cat /tmp/.docker.xauth
docker run --rm -it --net=host -p 8888:8888 -v /tmp/.docker.xauth:/tmp/.docker.xauth -v /tmp/.X11-unix:/tmp/.X11-unix --env DISPLAY=$DISPLAY --env XAUTHORITY=/tmp/.docker.xauth jkridner/crystal
