module Bootstrap
  class Cat
    @happy : Bool
    @name : String

    def initialize
      @happy = false
      @name = ""
    end

    def happy
      @happy
    end

    def name=(x : String)
      @name = x
    end

    def name
      @name
    end

    def pet
      puts "Someone pet: "
      puts @name
      @happy = true
    end
  end
end
