#!/usr/bin/env crystal
# See https://perens.com/2018/07/06/tiny-crystal-language-programs/
require "lib_c"
{% if flag?(:x86_64) %}
require "lib_c/x86_64-linux-gnu/c/stdlib"
require "lib_c/x86_64-linux-gnu/c/stdio"
{% elsif flag?(:aarch64) %}
require "lib_c/aarch64-linux-gnu/c/stdlib"
require "lib_c/aarch64-linux-gnu/c/stdio"
{% end %}

class String
  def to_unsafe
    pointerof(@c)
  end
end

def puts(txt)
  LibC.printf txt
  LibC.printf "\n"
end

require "./bootstrap"

cat = Bootstrap::Cat.new
cat.name = "Fluffy"
cat.pet
