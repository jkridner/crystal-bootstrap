#!/usr/bin/env -S crystal build --prelude="empty" -p --release --no-debug --static
# https://perens.com/2018/07/06/tiny-crystal-language-programs/
require "lib_c"
require "lib_c/x86_64-linux-gnu/c/stdlib"
require "lib_c/x86_64-linux-gnu/c/stdio"

#def free(object)
#  LibC.free(pointerof(object))
#end

class String
  def to_unsafe
    pointerof(@c)
  end
end

#class Foo
#  def bar
    LibC.printf "Hello, World!\n"
#  end
#end

#f = Foo.new
#f.bar
#free(f)
