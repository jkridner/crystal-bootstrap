#!/bin/sh
#crystal build src/main.cr --static --release --no-debug -p --error-trace --prelude ./prelude.cr --mcmodel kernel -Dgc_none -Dskip_crystal_compiler_rt -Dwithout_iconv
#crystal build src/main.cr --static --release --no-debug -p --error-trace --prelude ./prelude.cr --mcmodel kernel -Dskip_crystal_compiler_rt -Dwithout_iconv
#crystal build --prelude="empty" -p --release --no-debug --static src/main.cr
crystal build src/main.cr --prelude="empty" --static --release --no-debug -p --error-trace --mcmodel kernel -Dskip_crystal_compiler_rt -Dwithout_iconv
ls -l main
mv main crystal-bootstrap.exe
nm crystal-bootstrap.exe > crystal-bootstrap.symbols.txt
strip crystal-bootstrap.exe
ls -l crystal-bootstrap.exe
