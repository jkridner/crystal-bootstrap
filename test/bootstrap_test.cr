require "minitest/autorun"

require "/../src/bootstrap.cr"

class LearningTest < Minitest::Test
  def cat
    @cat ||= Bootstrap::Cat.new
  end

  def setup
    cat.name = "Bootsie"
  end

  def test_cat_has_adorable_name
    assert_equal "Bootsie", cat.name
  end

  def test_cat_likes_petting
    cat.pet
    assert cat.happy
  end
end
