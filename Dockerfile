FROM alpine:edge AS crystal
RUN echo '@edge http://dl-cdn.alpinelinux.org/alpine/edge/community' >>/etc/apk/repositories
RUN apk add --update --no-cache --force-overwrite \
  git \
  make \
  sudo \
  crystal@edge
RUN adduser unified -D \
  && mkdir -p /etc/sudoers.d \
  && echo '%wheel ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/wheel \
  && adduser unified wheel \
  && adduser unified abuild
USER unified
