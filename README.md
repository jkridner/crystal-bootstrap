# Crystal Bootstrap

This example should be suitable for learning about bootstraping the AM62 in any language.

## Interruption

As a step along the way, I am going to try to build a journaling application to better work with Crystal.

- [ ] Add a Docker image with a Crystal runtime environment
- [ ] Add a web browser and invokation for the browser GUI

## The rest

My objective is to place Crystal Language program source and a script in this repo code that can run on an aarch64 Linux machine that will:
- [ ] Generate a statically-linked aarch64 executable from a Crystal Language source.
- [ ] Apply a boot header with a suitable entry point for the executable.
- [ ] Place that executble into a FAT-formated uSD card image file.
- [ ] Format the disk image file to be bootable by the ROM code of the AM62 in internal memory.

Functionality will follow in roughly these phases:
- [ ] Toggle a GPIO with an LED attached to prove that the executable is running.
- [ ] Enable the DDR and perform a simple read/write test that toggles the LED to indicate pass/fail.
- [ ] Perform read and write operations to the uSD card and verify by removing the card and examining it on another computer.
- [ ] Perform operations on files via the FAT file system.
- [ ] Load an additional executable from the FAT file system into DDR and toggle an LED to prove it worked.
- [ ] Perform write operations to the console UART.
- [ ] Perform read/write operations to the console UART.
- [ ] Connect the Crystal STDIO operations (puts/gets) to the console UART.
